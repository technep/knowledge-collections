## Fido D2S

![alt soure:https://cdn.shopify.cn/s/files/1/0370/7489/4981/files/D2__02_6e34bcfc-0cfc-47f5-92cf-4667c3885d1d_1024x1024.jpg?v=1586771437](https://cdn.shopify.cn/s/files/1/0370/7489/4981/files/D2__02_6e34bcfc-0cfc-47f5-92cf-4667c3885d1d_1024x1024.jpg?v=1586771437)


## Controller WK3615YS

|   |   |
| ------ | ------ |
| Voltage | 36V |
| Phase Angle | 120° |
| Limit | 14±2A |
| Untervoltage | 29±2V |

## [Pedelec - Pedal Electric Cycle  / E-Bike25](https://www.e-bikeinfo.de/recht/gesetzliche-vorschriften-e-bikes-pedelecs)

1. [x] Pedal Assistance up to 25km/h 
2. [x] Maximum 250W Motor
3. [ ] Start-assist system (a type of throttle) without pedalling upto 6km/h
4. [x] [Certification of Compliance](https://img.gkbcdn.com/s3/d/202004/0bc5ce94-008f-4bde-8498-b118831d772f.jpg)
5. [x] [CE](https://img.gkbcdn.com/s3/d/202004/0bc5ce94-008f-4bde-8498-b118831d772f.jpg) 


## How to deactivate the trottle - Workaround

To make make Fido D2[s] E-Bike25 Compliant

### 1. Shopping List
1. 1 x [Linear magnetic hall effect sensor e.g OH49E SS49E](https://www.amazon.de/s?k=oh49e+ss49e&crid=1URFZS2UB4STU&sprefix=OH49E%2Caps%2C185&ref=nb_sb_ss_ts-ap-p_1_5)
2. [Heat Shrink Tube](https://www.amazon.de/dp/B071D7LJ31/ref=cm_sw_em_r_mt_dp_5YlBFbW7QQ6Q2)
    - 1x45mm 3 pcs
    - 4x45mm 1 pc
    - 10x45mm 1 pc
3. 1 x [JST SM 3PIN - Female](https://www.amazon.de/s?k=JST+SM+3PIN&ref=nb_sb_noss_2)

![img](https://gitlab.com/technep/knowledge-collections/-/raw/master/ebike/fido/img/1.jpeg)

### 2. Create a dummy throttle
::: tip
Connect a Wire with JST SM 3 Pin Female-Connector on the one side and Linear magnetic Hall Sensor on the other side as in the picture below
:::

![img](https://gitlab.com/technep/knowledge-collections/-/raw/master/ebike/fido/img/hallsensor.jpeg)

### 3. Open the controller compartment 
::: tip
Get access to the E-Bike Controller: Unscrew the top and bottom screw 
:::

![img](https://gitlab.com/technep/knowledge-collections/-/raw/master/ebike/fido/img/open.jpeg)

### 4. Throttle cable

![video](https://gitlab.com/technep/knowledge-collections/-/raw/master/ebike/fido/img/opennoaudio.mp4)

### 5. Disconnect current connection 
::: tip
Search the Black|Green|Red wire connected to the throttle and disconnect it. Now connect the Custom made JST SM3 connector with Hall-Sensor.
:::

![img](https://gitlab.com/technep/knowledge-collections/-/raw/master/ebike/fido/img/connect.jpeg)

::: danger !!! Caution !!!
Do not plug the controller to the battery as in the video
:::

![video](https://gitlab.com/technep/knowledge-collections/-/raw/master/ebike/fido/img/after.mp4)

### 6. Controller back in the compartment 
::: danger !!! Caution !!! 
Black Wire top, Red Wire Bottom !!!
:::

![video](https://gitlab.com/technep/knowledge-collections/-/raw/master/ebike/fido/img/afterclosenoaudio.mp4)


## [Still need some items to make Fido Road Traffic Act - StV0 Compliant](https://www.fahrrad-xxl.de/beratung/fahrrad/stvzo/)
1. [ ] [Rear Light](https://www.amazon.de/s?k=Rear+Light&ref=nb_sb_noss)
2. [x] Rear Reflector (Red)
3. [x] Brakes
4. [x] [Bell](https://www.amazon.de/s?k=bell+bycicle&crid=248KUF4NNSQJ5&sprefix=bell+byc%2Caps%2C172&ref=nb_sb_ss_ts-ap-p_1_8)
5. [ ] [Front Reflector (White)](https://www.amazon.de/s?k=Reflektor+vorne&ref=nb_sb_noss_2)
6. [x] Front light
7. [ ] [Reflector on the wheels (White)](https://www.amazon.de/s?k=Reflektor+vorne&ref=nb_sb_noss_2)
8. [x] Reflector on the paddels (Orange)






