# Testing

## Tools
- [Squish GUI Tester](https://www.froglogic.com/squish/) (Proprietary)


# Links

## Automotive

## User Interfaces

- [Automotive User Interfaces and Interactive Vehicular Applications](https://www.froglogic.com/squish/)  (2011)
- [15 of the best car dashboard designs](https://designsmaz.com/best-car-dashboard-designs/) (July 2015)
- [5 Car UX Trends](https://medium.com/@pvermaer/5-car-ux-trends-43e3ca2471ca) (Aug 2015)
- [A matter of convergence: building digital instrument clusters with Qt on QNX](http://qnxauto.blogspot.com/2016/05/a-matter-of-convergence-building.html) (2016)
- [The Future of Automotive UX from the Designer's Perspective](https://www.rightware.com/blog/the-future-of-automotive-ux-from-the-designers-perspective) (July 2019)
- [Why UX is booming in the automotive industry](https://uxdesign.cc/why-ux-is-booming-in-the-automotive-industry-6794eb599a51)
- [Car Dashboard Shots](https://dribbble.com/tags/car_dashboard) (Dec 2018)
- [Interface concept for Tesla in-car dashboard — a UX case study](https://uxdesign.cc/interface-concept-for-tesla-model-3-1b127384b472) (Feb 2019)

## Research Papers

- [Automotive User Experience Design Patterns: An Approach and Pattern Examples](https://www.researchgate.net/publication/317092583_Automotive_User_Experience_Design_Patterns_An_Approach_and_Pattern_Examples)
- [Cultural User Experience in the Car—Toward a Standardized Systematic Intercultural Agile Automotive UI/UX Design Process](https://www.researchgate.net/publication/314101915_Cultural_User_Experience_in_the_Car-Toward_a_Standardized_Systematic_Intercultural_Agile_Automotive_UIUX_Design_Process)
