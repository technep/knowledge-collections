# Bash

## SSH

### Create SSH-Keys

```bash
ssh-keygen -q -t rsa -b 4096 -C "${USER}@$(hostname)"
```

### Add SSH key to the ssh-agent

```bash
eval "$(ssh-agent -s)"
ssh-add ~/.ssh/id_rsa
```

### Connect to Remote server using ssh-key

```bash
ssh -i ~/.ssh/id_rsa $USER@server
```

### Access remote port 8082 via SSH-proxy
```bash
USERNAME_SERVER_1="USER1"
USERNAME_SERVER_2="USER2"
IP_SERVER_2_FROM_SERVER_1="192.168.3.12"
REMOTE_PORT=8082

echo "
Host server1
        HostName server1
        IdentityFile /home/${USER}/.ssh/id_rsa
        User ${USERNAME_SERVER_1}

Host server-2
        HostName "${IP_SERVER_2_FROM_SERVER_1}"
        IdentityFile ~/.ssh/id_rsa
        LocalForward ${REMOTE_PORT} localhost:${REMOTE_PORT}
        User ${USERNAME_SERVER_2}
        ProxyJump server1
" >> ~/.ssh/config
```