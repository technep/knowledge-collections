module.exports = {
      title: 'Collections of Knowledge',
      description: 'Knowledge collection during working...',
      themeConfig: {
            sidebar: 'auto'
      },
      dest:"public",
      base:"/knowledge-collections/",
      markdown: {
      extendMarkdown: md => {
            md.use(require('markdown-it-html5-embed'), {
            html5embed: {
            useImageSyntax: true,
            useLinkSyntax: false
            }
            })
      }
      }
}
