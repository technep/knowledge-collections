# CPP

## Useful functions

### Dump any Object

```cpp
#include <iostream>
#include <fstream>
#include <any>

void dumpObj2File( const std::string& file_name, 
                    const std::any& a, 
                    size_t bytes){
    std::ofstream f (file_name, std::ios::binary);
    f.write((char*)&a, bytes);
    f.close();

    // std::any_cast<T>(a) to read back
}

```

### Smart Pointer to avoid Memory leaks
```cpp
#include <iostream>
#include <vector>

struct SomeObject{
    SomeObject(int n){
        a = n;
        std::cout << "Constructor SomeObject called"
    }
    ~SomeObject(){
        std::cout << "Destructor SomeObject called"
    }
    int a  = 0;
};

int main(){
    
    std::vector<std::unique_ptr<SomeObject>> vecObjects;

    #Create some objects 
    vecObjects.push_back(std::make_unique<SomeObject>(5));

    vecObjects.clear();
}

```

### Global Variables

```cpp
//global.h
#pragma once
inline int a = 0;


#include <iostream>
#include "global.h"

int main(){
    std:.cout << "Main:" << a << std::endl;
    return 0;
}

```
