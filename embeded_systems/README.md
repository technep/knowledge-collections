# Embeded Systems

## Custom Liunx OS
### Setup First Environment Variables
```bash
#!/bin/bash
export SUITE="jessie"
export VARIANT="minbase" #minbase|buildd|fakechroot
export MACHINE_ARCH="armhf"
export MIRROR_URI="http://ftp.debian.org/debian/"


export CHROOT_DIR="/home/${USER}/${MACHINE_ARCH}-${OS_SID}"

```

### [Debootstrap](https://wiki.debian.org/Debootstrap)
debootstrap is a tool which will install a Debian base system into a subdirectory of another, already installed system. It doesn't require an installation CD, just access to a Debian repository. It can also be installed and run from another operating system, so, for instance, you can use debootstrap to install Debian onto an unused partition from a running Gentoo system. It can also be used to create a rootfs for a machine of a different architecture, which is known as "cross-debootstrapping".

#### Install

```bash
sudo apt-get install \
	debootstrap \
	qemu-debootstra \
	qemu-user-static 

```

#### Run

```bash
#!/bin/bash

# First Phase
qemu-debootstrap \
	--arch="${MACHINE_ARCH}" \
	--variant="${VARIANT}" \
	"${SUITE}" \
	"${CHROOT_DIR}" \
	${MIRROR_URI}
```


### [Schroot](https://wiki.debian.org/Schroot)
Schroot allows users to execute commands or interactive shells in different chroots

#### Install

```bash
#!/bin/bash
sudo apt-get install \
	schroot
```

#### Create configuration file

```bash
#!\bin\bash

echo "[${MACHINE_ARCH}-${OS_SID}]
description=${OS_SID} (${MACHINE_ARCH})
directory=${CHROOT_DIR}
root-users=$(whoami)
users=$(whoami)
type=directory" | sudo tee /etc/schroot/chroot.d/${MACHINE_ARCH}-${OS_SID}

```

#### Run
```bash
#!\bin\bash

schroot -c "${MACHINE_ARCH}-${OS_SID}"

```


### [Yocto](https://www.yoctoproject.org/) 
The Yocto Project (YP) is an open source collaboration project that helps developers create custom Linux-based systems regardless of the hardware architecture.

The project provides a flexible set of tools and a space where embedded developers worldwide can share technologies, software stacks, configurations, and best practices that can be used to create tailored Linux images for embedded and IOT devices, or anywhere a customized Linux OS is needed. 

#### 1. Build Host Packages
```bash
sudo apt-get install \
	gawk wget git-core diffstat \
	unzip texinfo gcc-multilib \
    build-essential chrpath socat \
	cpio python3 python3-pip python3-pexpect \
    xz-utils debianutils iputils-ping \
	python3-git python3-jinja2 \
	libegl1-mesa libsdl1.2-dev \
    pylint3 xterm
            
```

#### 2. Clone Poky anch select branch (krogoth) e.g pi3
```bash
mkdir -p ~/rpi/sources
cd ~/rpi/sources
git clone -b krogoth git://git.yoctoproject.org/poky
git clone -b krogoth git://git.openembedded.org/meta-openembedded
git clone -b krogoth git://git.yoctoproject.org/meta-raspberrypi
            
```

#### 3. Init Build Env and create Image
```bash
cd ~/rpi/
source sources/poky/oe-init-build-env rpi-build
```

#### 4. Modify config (local.conf)
```bash
PATH_LOCAL_CONF=~/rpi/rpi-build/conf/local.conf
echo 'MACHINE = "raspberrypi3"' >> "${PATH_LOCAL_CONF}"
echo 'PREFERRED_VERSION_linux-raspberrypi = "4.%"' >> "${PATH_LOCAL_CONF}"
echo 'DISTRO_FEATURES_remove = "x11 wayland"' >> "${PATH_LOCAL_CONF}"
echo 'DISTRO_FEATURES_append = " systemd"' >> "${PATH_LOCAL_CONF}"
echo 'VIRTUAL-RUNTIME_init_manager = "systemd"' >> "${PATH_LOCAL_CONF}"
```

#### 5. Modify bblayers (local.conf)
```bash

PATH_BBLAYERS_CONF="~/rpi/rpi-build/bblayers.conf"
echo '# LAYER_CONF_VERSION is increased each time build/conf/bblayers.conf
# changes incompatibly
POKY_BBLAYERS_CONF_VERSION = "2"

BBPATH = "${TOPDIR}"
BBFILES ?= ""
' > "${PATH_BBLAYERS_CONF}"

echo "BSPDIR := \"/home/${USER}/rpi/\"" > "${PATH_BBLAYERS_CONF}"

echo '
BBLAYERS ?= " \
  ${BSPDIR}/sources/poky/meta \
  ${BSPDIR}/sources/poky/meta-poky \
  ${BSPDIR}/sources/poky/meta-yocto-bsp \
  ${BSPDIR}/sources/meta-openembedded/meta-oe \
  ${BSPDIR}/sources/meta-openembedded/meta-multimedia \
  ${BSPDIR}/sources/meta-raspberrypi \
  "
BBLAYERS_NON_REMOVABLE ?= " \
  ${BSPDIR}/sources/poky/meta \
  ${BSPDIR}/sources/poky/meta-poky \
  "
' >> "${PATH_BBLAYERS_CONF}"
```

#### 6. Build Image

```bash
bitbake rpi-basic-image
```

#### 7. Image to SD-CARD

```bash
sudo dd \
if=~/rpi/rpi-build/tmp/deploy/images/raspberrypi3/ \
of=/dev/sdX bs=4M \
rpi-basic-image-raspberrypi3.rpi-sdimg && sync

```
